'use strict';

angular.module('myWebApp')
.controller('HomeController',['$scope','homeFactory',function($scope, homeFactory){
    
    $scope.homeData = homeFactory.getHomeData();
    
}])

.controller('HeaderController',['$scope','$state',function($scope, $state){
    
    $scope.stateis = function(curstate){
        return $state.is(curstate);
    };
    
    
}])

.controller('PlaygroundController',['$scope', function($scope){
    
    $scope.test = "Hello Data";
    
    $scope.domtest = "<h2>This is awesome</h2>";    
    
       
   $scope.handleDrop = function(){
       console.log("I am in play controller");
       alert('Item has been dropped');
   }
   
}])



;