'use strict';

angular.module('myWebApp',['ui.router', 'ngSanitize'])

    .config(function($stateProvider,$urlRouterProvider){
    
    $stateProvider
    .state('app',{
        url:'/',
        views:{
            'header':{
                templateUrl:'app/views/header.html',
                controller:'HeaderController'
            },
            'content':{
                templateUrl:'app/views/home.html',
            },
            'footer':{
                templateUrl:'app/views/footer.html'                
            }
        }
    })    
    .state('app.about',{
        url:'about',
        views:{            
            'content@':{
                templateUrl:'app/views/about.html'          
            }
        }
    })
    
    .state('app.mywork',{
        url:'mywork',
        views:{
            'content@':{
                templateUrl:'app/views/mywork.html'
            }
        }
    })
    
    .state('app.playground',{
        url:'playground',
        views:{            
            'content@':{
                templateUrl:'app/views/playground.html',
                controller:'PlaygroundController'
            }
        }
    })
    
    .state('app.contact',{
        url:'contact',
        views:{            
            'content@':{
                templateUrl:'app/views/contact.html'
            }
        }
    });
    
    $urlRouterProvider.otherwise('/');
})

;