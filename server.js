var express = require('express'),
	bodyParser = require('body-parser');
//	_ = require('lodash');
var morgan = require('morgan');
var	app = express();

var PORT = 80;

var hostname = "139.59.34.115";

app.use(morgan('dev'));
// EXPRESS CONFIG
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/'));

// ROUTES
app.get('/', function(req, res){
    console.log("I am in get/");
	res.sendFile('index.html');
});

// Start server
app.listen(PORT, hostname,function(){
  console.log('Server listening on port ' + PORT)
});