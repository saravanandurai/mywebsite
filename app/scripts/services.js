'use strict';

angular.module('myWebApp')
    .factory('homeFactory',function(){
    
    var homedata = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu mi vitae sem venenatis luctus. Integer imperdiet arcu ac justo vestibulum, eget feugiat mi ultricies. Quisque blandit urna ut quam sodales, id rhoncus ligula semper. Pellentesque in magna velit. Donec quam orci, sodales molestie enim id, ullamcorper malesuada augue. Pellentesque at nisl consectetur, ornare odio at, rhoncus lacus. Morbi faucibus feugiat lobortis. Morbi sagittis vehicula orci, tincidunt pellentesque dolor maximus ac. Mauris lorem enim, vulputate et iaculis quis, venenatis id felis. Phasellus id dui euismod, tincidunt ligula ut, convallis magna. Integer id tellus nec sapien ultricies sollicitudin. Sed fermentum fringilla odio quis cursus.Sed tincidunt faucibus convallis. Etiam imperdiet, tortor ut interdum ullamcorper, tellus leo egestas tellus, vitae varius tortor mauris non lectus. Curabitur non arcu nec metus elementum maximus. Morbi at ante nisl. Integer convallis non nisl semper suscipit. Donec dapibus ipsum eget elit mollis, sed viverra turpis sagittis. Duis nec efficitur nulla. Integer iaculis elementum metus. Proin ullamcorper tristique justo ut vulputate. Mauris non volutpat ligula, ut lobortis quam. Aenean imperdiet mi leo, vitae fermentum massa semper quis. Pellentesque aliquet auctor dapibus. Aliquam dictum ornare neque, nec congue neque rutrum non. Quisque condimentum rhoncus risus in euismod. Curabitur ut congue nisl. Sed molestie quis orci vitae rutrum.";
    
    var homeFac = {};
    
    homeFac.getHomeData = function(){
      return homedata;  
    };
    
    return homeFac;
    
});